package pl.mateuszstefanski.dto;

import pl.mateuszstefanski.entity.Product;
import pl.mateuszstefanski.entity.Receipt;

public interface DtoMapper<T, D> {

    D mapFromDomainToDto(T domainObject);

    T mapFromDtoToDomain(D dtoObject);
}
