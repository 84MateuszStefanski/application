package pl.mateuszstefanski.dto;

import jakarta.persistence.CascadeType;
import jakarta.persistence.OneToMany;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.mateuszstefanski.entity.Product;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
@Getter
@Builder
public class ReceiptDto {

    private LocalDateTime date;
    private String shopName;
    private List<ProductDto> products;
    private BigDecimal sum;

}
