package pl.mateuszstefanski.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@Getter
@Builder
public class ProductDto {

    private String name;

    private BigDecimal pricePerUnit;

    private Double quantity;

    private BigDecimal sum;

}
