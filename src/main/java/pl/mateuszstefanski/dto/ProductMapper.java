package pl.mateuszstefanski.dto;

import org.springframework.stereotype.Service;
import pl.mateuszstefanski.entity.Product;

@Service
public class ProductMapper implements DtoMapper<Product, ProductDto> {

    @Override
    public ProductDto mapFromDomainToDto(final Product product) {
        return ProductDto.builder()
                .name(product.getName())
                .pricePerUnit(product.getPricePerUnit())
                .quantity(product.getQuantity())
                .sum(product.getSum())
                .build();
    }

    @Override
    public Product mapFromDtoToDomain(final ProductDto productDto) {
        return Product.builder()
                .name(productDto.getName())
                .pricePerUnit(productDto.getPricePerUnit())
                .quantity(productDto.getQuantity())
                .sum(productDto.getSum())
                .build();
    }

}
