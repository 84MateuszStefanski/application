package pl.mateuszstefanski.dto;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.entity.Receipt;

import java.util.stream.Collectors;

@Service
public class ReceiptMapper implements DtoMapper<Receipt, ReceiptDto> {

    private ProductMapper productMapper;

    @Autowired
    ReceiptMapper(final ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    @Override
    public ReceiptDto mapFromDomainToDto(final Receipt receipt) {
        return ReceiptDto.builder()
                .date(receipt.getDate())
                .shopName(receipt.getShopName())
                .products(receipt.getProducts().stream().map(product -> productMapper.mapFromDomainToDto(product)).collect(Collectors.toList()))
                .sum(receipt.getSum())
                .build();
    }

    @Override
    public Receipt mapFromDtoToDomain(final ReceiptDto receiptDto) {
        return Receipt.builder()
                .date(receiptDto.getDate())
                .shopName(receiptDto.getShopName())
                .products(receiptDto.getProducts().stream().map(productDto -> productMapper.mapFromDtoToDomain(productDto)).collect(Collectors.toList()))
                .sum(receiptDto.getSum())
                .build();
    }
}
