package pl.mateuszstefanski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mateuszstefanski.entity.Receipt;

@Repository
public interface SqlReceiptRepository extends JpaRepository<Receipt, Long> {

}
