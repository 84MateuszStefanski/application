package pl.mateuszstefanski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mateuszstefanski.entity.Product;

@Repository
public interface SqlProductRepository extends JpaRepository<Product, Long> {

}
