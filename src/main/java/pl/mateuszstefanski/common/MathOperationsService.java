package pl.mateuszstefanski.common;

import java.math.BigDecimal;

public class MathOperationsService {

    public static BigDecimal countSum(final BigDecimal price, final Double quantity) {
        return price.multiply(BigDecimal.valueOf(quantity));
    }
}
