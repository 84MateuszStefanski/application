package pl.mateuszstefanski.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mateuszstefanski.dto.ReceiptDto;
import pl.mateuszstefanski.service.ReceiptService;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "receipt")
public class ReceiptController {

    private final ReceiptService receiptService;

    @PostMapping
    public void addNew(ReceiptDto receiptDto) {
        receiptService.addNew(receiptDto);
    }
}
