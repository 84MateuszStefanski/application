package pl.mateuszstefanski.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mateuszstefanski.dto.ProductDto;
import pl.mateuszstefanski.service.ProductService;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "product")
public class ProductController {

    private final ProductService productService;

    @PostMapping
    public void addNew(ProductDto productDto) {
        productService.addNew(productDto);
    }
}
