package pl.mateuszstefanski.Application;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("")
public class MainView extends VerticalLayout {




    public MainView() {
        H1 welcomeLabel = new H1("Witaj Vaadin");
        add(welcomeLabel);

        Button przycisk = new Button("Przejdź do kolejnego widoku");
        przycisk.addClickListener(event -> {
            getUI().ifPresent(ui -> ui.navigate("new-receipt"));
        });

        add(przycisk);

    }

}
