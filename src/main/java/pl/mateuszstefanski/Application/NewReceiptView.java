package pl.mateuszstefanski.Application;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

@Route(value = "new-receipt")
public class NewReceiptView extends VerticalLayout {

    public NewReceiptView() {
        H1 newReceipt = new H1("Enter your expense");
        add(newReceipt);
        TextField textField = new TextField();
        textField.setLabel("Name");
        textField.setClearButtonVisible(true);
        textField.setPrefixComponent(VaadinIcon.MAP_MARKER.create());
        add(textField);
    }
}
