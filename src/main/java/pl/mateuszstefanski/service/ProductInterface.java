package pl.mateuszstefanski.service;

import pl.mateuszstefanski.dto.ProductDto;

public interface ProductInterface extends ApplicationInterface<ProductDto> {

}
