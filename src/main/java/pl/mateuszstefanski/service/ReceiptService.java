package pl.mateuszstefanski.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.dto.ReceiptDto;
import pl.mateuszstefanski.dto.ReceiptMapper;
import pl.mateuszstefanski.repository.SqlReceiptRepository;

@RequiredArgsConstructor
@Service
public class ReceiptService implements ReceiptInterface{

    private static final Logger LOG = LoggerFactory.getLogger(ReceiptService.class);

    SqlReceiptRepository receiptRepository;
    ReceiptMapper receiptMapper;

    @Override
    public void addNew(final ReceiptDto receiptDto) {
        LOG.debug("New receipt is being created.");
        receiptRepository.save(receiptMapper.mapFromDtoToDomain(receiptDto));
    }
}
