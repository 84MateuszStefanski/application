package pl.mateuszstefanski.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.dto.ProductDto;
import pl.mateuszstefanski.dto.ProductMapper;
import pl.mateuszstefanski.repository.SqlProductRepository;

@RequiredArgsConstructor
@Service
public class ProductService implements ProductInterface{

    SqlProductRepository productRepository;
    ProductMapper productMapper;

    @Override
    public void addNew(final ProductDto productDto) {
        productRepository.save(productMapper.mapFromDtoToDomain(productDto));
    }

}
